FROM nginx

LABEL MAINTAINER="Roger W. Rocha <rwrock@gmail.com>"
LABEL APP_VERSION="1.0.0"

WORKDIR /usr/share/nginx/html/

COPY ./index.html index.html

EXPOSE 80